# renovate-config

This repository contains shareable renovate config presets for the Kubitus project.

More info [here](https://docs.renovatebot.com/config-presets/#gitlab-hosted-presets).
